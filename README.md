### Manifold learning in R

To install the package, run the following codes in R:
```
source("https://bioc.ism.ac.jp/biocLite.R")
biocLite("RDRToolbox")

devtools::install_github("kcf-jackson/maniTools", build_vignettes = TRUE)
browseVignettes("maniTools")
```
